<?php
session_start();

if(!isset($_SESSION['counter'])){
    $_SESSION['counter'] = 0;
}else{
    $_SESSION['counter'] ++;
}
?>
<p>
Olá visitante, você acessou esta página <?php echo $_SESSION['counter']; ?> vezes.
</p>

<p>
Para continuar, <a href="nextpage.php?<?php echo htmlspecialchars(SID); ?>">clique
aqui</a>.
</p>

