<?php
header("Content-Type: text/html; charset=utf-8");
echo "<h1>Continue e Break:</h1><hr>";

echo "O comando break interrompe a execução de um laço.<br/>";
echo "O comando continue apenas pula a iteração atual.<br/>";
echo "<hr>";

for($i=1; $i<30; $i++){
    if($i == 15){
        break;
    }

    echo $i.' - ';
}

