<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Usando Strings</title>
	<style type="text/css" media="screen">
		body{
			font-family: arial, helvetica;
			font-weight: bold;
			font-size:30px;
			color: #00f;
		}	
	</style>
</head>
<body>
	<?php
		echo "<h1 style='color:red; text-align:center;'>Variaveis do tipo String</h1>";
		echo '<hr/>';
		$nome = "Antonio";
		$sobrenome = "Alves";
		echo "Meu nome é $nome $sobrenome"; //com aspas duplas o php interpreta as referencias das variaveis

	?>
</body>
</html>