<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Usando Strings</title>
	<style type="text/css" media="screen">
		body{
			font-family: arial, helvetica;
			font-weight: bold;
			font-size:30px;
			color: #00f;
		}	
	</style>
</head>
<body>
	<?php
		$tabela = [
			1=>[1=>"codigo",  2=>"cidade", 3=>"estado"],
			2=>[1=>"1",2=>"Sapiranga", 3=>"RS"],
			3=>[1=>"2", 2=>"Campo Bom", 3=>"RS"]
		];

		//var_dump($tabela);
		print_r($tabela);

		echo "<h3 style = 'color: red;'>".$tabela[2][2]."</h3>";
	?>
</body>
</html>