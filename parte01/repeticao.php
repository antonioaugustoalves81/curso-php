<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Usando Strings</title>
	<style type="text/css" media="screen">
		body{
			font-family: arial, helvetica;
			font-weight: bold;
			font-size:20px;
			color: #00f;
			background-color: #e3e3e3;

		}	
	</style>
</head>
<body>
	<div style="border:2px solid #f00" id="lacos">
		<h1>Laço For com PHP</h1>
		<?php
			for($i=0; $i<=10; $i++){
				echo " [  $i  ] ";
			}
		?>
	</div>
	<hr/>

	<div style="border:2px solid #f00; color:#003" id="lacos">
		<h1>Laço While com PHP</h1>
		<?php
			$j = 0;

			while($j <= 10){
				echo " [ $j ] ";
				$j++;
			}
		?>
	</div>

</body>
</html>