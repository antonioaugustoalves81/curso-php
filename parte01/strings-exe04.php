<?php
//FUNÇÕES PARA TRATAMENTO DE STRINGS.
$str1 = 'abc';
$str2 = 'def';
//strcmp retorna -1 quando as strings são diferentes
echo "Resultado para $str1 e $str2 : ".strcmp($str1,$str2)."<br/>";

$str1 = 'abc';
$str2 = 'ABC';
//strcmp retorna 1 quando as strings são iguais mas com caixa diferente
echo "Resultado para $str1 e $str2 : ".strcmp($str1,$str2)."<br/>";

$str1 = 'ABC';
$str2 = 'ABC';
//strcmp retorna 0 quando as strings são exatamente iguais
echo "Resultado para $str1 e $str2 : ".strcmp($str1,$str2)."<br/>";








