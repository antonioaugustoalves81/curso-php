<?php
function contarDias($dataInicial, $dataFinal){
	//timestamp das datas
	$ts_inicial = strtotime($dataInicial);
	$ts_final = strtotime($dataFinal);

	$diferenca = $ts_final - $ts_inicial;

	return (int)floor($diferenca/(60*60*24)); //
}

echo "Quantos dias desde o inicio do ano? ". contarDias("2018-01-01", "2018-06-19")." dias.<br><hr>";

echo "Faltam ".contarDias("2018-06-19","2018-12-23")." dias para o meu aniversário!<br><hr>";
?>