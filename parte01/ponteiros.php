<?php
$cursos = array('HTML', 'CSS', 'JAVASCRIPT', 'PHP','JAVA');
/****
var_dump(current($cursos));
var_dump(next($cursos));
var_dump(key($cursos));
var_dump(end($cursos));
var_dump(prev($cursos));
****/
function imprimeLista($array){
	reset($array); //1º indice
	while(key($array) !== null){
		echo key($array)." => ".current($array)."<br/><hr/>";
		next($array);
	}
}

imprimeLista($cursos);
echo "<h2>Usando o laço ForEach</h2>";

forEach($cursos as $cod => $curso){
	echo "$cod ) $curso <br/><hr/>";

}
?>