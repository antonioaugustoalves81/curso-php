<?php
	echo "<h2>Formato para timezones.</h2>";
	date_default_timezone_set("America/Sao_Paulo");
	$fuso = date("e");
	echo "Timezone: ".$fuso."<br/>";

	$fuso = date("I");
	if($fuso == 0){
		echo "Horario normal <br/>";
	}else{
		echo "Horario de verão <br/>";
	}
	echo "Timezone: ".$fuso."<br/>";

	$fuso = date("O");
	echo "Timezone: ".$fuso."<br/>";

	$fuso = date("P");
	echo "Timezone: ".$fuso."<br/>";

	$fuso = date("T");
	echo "Timezone: ".$fuso."<br/>";

	$fuso = date("Z");
	echo "Timezone: ".$fuso."<br/>";

	echo "<h2>Formato ISO para horas.</h2>";
	$hora = date("c");
	echo "Hora: ".$hora."<br/>";
	$hora = date("r");
	echo "Hora: ".$hora."<br/>";
	$hora = date("U");
	echo "Segundos desde 1/1/1970: ".$hora."<br/>";

	$tempo = time();
	echo "Função time(): ".$tempo."<br><hr>";

	echo "<h2>Mexendo com horas.</h2>";
	$hr = date("a");
	echo "<b>am ou pm:</b>".$hr."<br>";

	$hr = date("A");
	echo "<b>AM ou PM:</b>".$hr."<br>";

	$hr = date("g");
	echo "<b>Formato 12 horas:</b>".$hr."<br>";
	$hr = date("G");
	echo "<b>Formato 24 horas:</b>".$hr."<br>";

	$hr = date("h");
	echo "<b>Formato 12 horas com zero:</b>".$hr."<br>";
	$hr = date("H");
	echo "<b>Formato 24 horas com zero:</b>".$hr."<br>";

	$hr = date("i");
	echo "<b>Minutos com zero:</b>".$hr."<br>";
	$hr = date("s");
	echo "<b>Segundos com zero:</b>".$hr."<br>";
	$hr = date("u");
	echo "<b>Milisegundos:</b>".$hr."<br>";
	$hr = date("H:i:s");
	echo "<b>Hora atual:".$hr."</b><br/>"
	


?>