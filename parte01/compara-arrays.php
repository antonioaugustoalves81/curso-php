<?php
$a = array(1,2,3);
$b = array(1=>2,2=>3,0=>1);
$c = array('a'=>1, 'b'=>2, 'c'=>3);

//mesmos pares chave/valor mas fora de ordem
var_dump($a == $b);

var_dump($a === $b);//false

$c1 = array('PHP', 'PYTHON', 'R', 'JAVA','JAVASCRIPT');
$c2 = array('PHP', 'PYTHON', 'R');
$res = array_diff($c1, $c2);
print_r($res);

var_dump(count($c1));
var_dump(isset($a));
var_dump(isset($c1[1]));

//Existe o elemento X no array?
var_dump(in_array('JAVA', $c1));
?>