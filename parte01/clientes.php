<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Usando Strings</title>
	<style type="text/css" media="screen">
		body{
			font-family: arial, helvetica;
			font-weight: bold;
			font-size:30px;
			color: #00f;
		}	
	</style>
</head>
<body>
	<?php
		$clientes = [
			['nome'=>'Antonio', 'endereco'=>'Rua João Corrêa, 21',
			'telefones'=>['fixo'=>'3599-2211', 'celular'=>'999-545-232']],

			['nome'=>'Mariana', 'endereco'=>'Rua Sete de Setembro, 21',
			'telefones'=>['fixo'=>'3599-3433', 'celular'=>'998-675-111']]
		];

		echo $clientes[0]['nome'].": ";
		echo $clientes[0]['telefones']['celular'];
	?>
</body>
</html>