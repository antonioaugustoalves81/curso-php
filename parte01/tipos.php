<?php
header('Content-Type: text/html; charset=utf-8');
  // Declaração das variáveis

    $v_string = "TreinaWeb Cursos";
    $v_boolean = TRUE;
    $v_integer = 100;
    $v_float = 100.9;

    // Impressão dos tipos

    echo gettype($v_float); // Retorna o tipo da variável.

    var_dump($v_float); // Retorna o tipo e o valor da variável

    var_dump( $v_boolean ); // Retorna o tipo e o valor da variável

    // Verificação de tipos

    if( is_int($v_integer) ){
      echo '$v_integer é um inteiro. <br />';
    }

    if( !is_bool($v_string) ){
      echo '$v_string não é um valor booleano.';
    }

?>