<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Usando um Select com foreach</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<?php
		$cursos = [
			1 =>'PHP',
			2 =>'JAVA',
			3 =>'PYTHON',
			4 =>'JAVASCRIPT',
			5 =>'PL/SQL'
		];
		?>
		<select id="cursos">
			<?php
			foreach($cursos as $key => $curso){
			?>
			<option value="<?php echo $key;?>"><?php echo $curso;?></option>
			<?php
			}
			?>
		</select>
		<button id="selecionar">Selecionar Curso</button>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function (){
		$('#cursos').change(function (){
			var c = $("#cursos option:selected").val();
			console.log("Curso :"+c);
			var txt = $("#cursos option:selected").text();

			alert("Curso Selecionado: "+txt);
		});
	});
	</script>
			
</body>
</html>