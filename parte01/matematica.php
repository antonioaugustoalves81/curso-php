<?php
	$amostra = array(2,15,16,22,23,17,18,18,18,19);
	$menor = min($amostra);
	$maior = max($amostra);
	print_r($amostra);
	echo "<br>";
	echo "Maior valor do array: $maior.<br/>";
	echo "Menor valor do array: $menor.<br/>";

	$soma = 0;
	$quant = count($amostra);
	forEach($amostra as $chave=>$valor){
		$soma+=$valor;
	}

	$media = $soma/$quant;
	echo "Soma: $soma <br>";
	echo "Tamanho da amostra: $quant <br>";
	echo "Média aritimética: $media <br>";

	echo "<h3>Funções de arredondamento:</h3>";
	echo "Função round(): ".round($media, 2)."<br>";//numero de casa opcional.
	echo "Função ceil(): ".ceil($media)."<br>";
	echo "Função floor(): ".floor($media)."<br>";
	echo "<hr>";
	echo "<h3>Gerador de números randômicos</h3>";

	$val = mt_rand(1,60);
	echo "Número sorteado: $val";
?>