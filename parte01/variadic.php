<?php
//Variadic recurso do php 5.6 que permite criação de funções mais genericas

function media(){
	$valores = func_get_args();
	$soma = 0;

	foreach($valores as $valor){
		$soma = $soma + $valor;
	}

	$media = $soma/count($valores);

	return $media;
}

function imprime(){
	$params = func_get_args();

	foreach($params as $p){
		echo "$p <br/>";
	}
	echo "<br><hr>";
}


echo "Média das notas: ".media(8,9,4,6)."<br>";
echo "Média das alturas: ".media(1.78,1.89,1.64,1.56, 1.72, 1.88, 1.91)."<br><hr>";
imprime('A','B','C');
imprime('Jan','Fev','Mar','Abr','Mai');
?>