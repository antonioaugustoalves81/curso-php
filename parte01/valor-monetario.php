<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Number format</title>
	<style type="text/css" media="screen">
		body{
			font-family: arial, helvetica;
			font-weight: bold;
			font-size:30px;
			color: #00f;
		}	
	</style>
</head>
<body>
	<?php
		//http://php.net/manual/en/ref.gmp.php
		$saldo = 1500.00;
		$saque = 250.00;
		$saque += $saque * 0.15; 

		$resultado =  $saldo - $saque;

		echo 'R$ '.number_format($resultado, 2, ',', '.');

	?>
</body>
</html>