<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Usando Strings</title>
	<style type="text/css" media="screen">
		body{
			font-family: arial, helvetica;
			font-weight: bold;
			font-size:20px;
			color: #00f;
			background-color: #e3e3e3;

		}	
	</style>
</head>
<body>
	<?php
		$n1 = 11;
		$n2 = 7;
		$n3 = 1;

		if($n1 > $n2 && $n1 > $n3){
			echo "N1 é maior";
		}elseif($n2>$n1 && $n2>$n3){
			echo "N2 é maior";
		}else{
			echo "N3 é maior";
		}
		?>
		<br/><br/>
		<hr/>
		<?php
		if($n1 < $n2 && $n1 < $n3){
			echo "N1 é menor";
		}elseif($n2<$n1 && $n2<$n3){
			echo "N2 é menor";
		}elseif ($n3<$n1 && $n3<$n2){
			echo "N3 é menor";
		}
	?>
	<br/><br/>
	<hr/>
	<?php
		if($n1 > $n2 && $n1 < $n3 || $n1 < $n2 && $n1 > $n3){
			echo "N1 é o valor do meio";
		}elseif($n2>$n1 && $n2<$n3 || $n2<$n1 && $n2>$n3){
			echo "N2 é o valor do meio";
		}elseif ($n3>$n1 && $n3<$n2 || $n3<$n1 && $n3>$n2){
			echo "N3 é o valor do meio";
		}
	?>

	<h3 style = "color:green;">Usando o switch / case</h3>
	<hr/>
	<?php
		$n = 1;
		echo "O número é: $n <br/>";
		switch($n){
			case 0:
			echo "Zero";
			break;

			case 1:
			echo "Um";
			break;

			case 2:
			echo "Dois";
			break;

			case 3:
			echo "Três";
			break;

			case 4:
			echo "Quatro";
			break;

			default:
			echo "Outro numero";
			break;


		}
	?>
</body>
</html>