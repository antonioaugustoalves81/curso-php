<?php
$cursos = array('HTML', 'CSS',
	'JAVASCRIPT', 'PHP','JAVA',
	'MYSQL','ANGULAR','REACT','IONIC',
	'XAMARIN','PYTHON','C#');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Exemplo de formulário:</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form action="processa-cursos.php" method="get">
		<fieldset>

			<?php forEach($cursos as $cod=>$curso):?>
			<p>
				<input type="checkbox" name="cursos[]" id="c<?=$cod?>" value = "<?=$curso?>"/>
				<label for="nome"><?=$curso?></label>
			</p>
			<?php endforeach;?>
			<p><input type="submit" value="Confirmar"></p>
		</fieldset>

	</form>
</body>
</html>