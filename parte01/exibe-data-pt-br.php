<?php
	function exibeSaudacao($formato = 'extenso'){
		/***************************
		validando o formato
		****************************/

		$formatosValidos = array('extenso', 'abreviado');

		if(!in_array($formato, $formatosValidos)){
			return "Formato inválido!";
		}//Fim da validação de formato

		$diaSemana = array(
		1=>array(
			'extenso'=>'Segunda-Feira',
			'abreviado'=>'Seg'),
		2=>array(
			'extenso'=>'Terça-Feira',
			'abreviado'=>'Ter'),
		3=>array('extenso'=>'Quarta-Feira',
				'abreviado'=>'Qua'),
		4=>array('extenso'=>'Quinta-Feira',
				'abreviado'=>'Qui'),
		5=>array('extenso'=>'Sexta-Feira',
				'abreviado'=>'Sex'),
		6=>array('extenso'=>'Sábado',
				'abreviado'=>'Sáb'),
		0=>array('extenso'=>'Domingo',
				'abreviado'=>'Dom')
	);

	return $diaSemana[date('w')][$formato];
	}

	function exibeDataExtenso(){
		$diaSemana = array(
			1=>'Segunda-Feira',
			2=>'Terça-Feira',
			3=>'Quarta-Feira',
			4=>'Quinta-Feira',
			5=>'Sexta-Feira',
			6=>'Sábado',
			0=>'Domingo'
		);

		$mesAno = array(
			1=>'Janeiro',
			2=>'Fevereiro',
			3=>'Março',
			4=>'Abril',
			5=>'Maio',
			6=>'Junho',
			7=>'Julho',
			8=>'Agosto',
			9=>'Setembro',
			10=>'Outubro',
			11=>'Novembro',
			12=>'Dezembro'
		);

		Return "Hoje é ".$diaSemana[date('w')].", dia ".date('d')." de ".$mesAno[date('n')]." de ".date('Y');
	}

	echo "Olá, hoje é ". exibeSaudacao('extenso')."<br>";
	echo "Olá. ".exibeDataExtenso()."<br>";

?>