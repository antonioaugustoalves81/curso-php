<?php
function somar($n1, $n2){
	$total = $n1+$n2;
	return $total;
}


function formatarValor($valor){
	return "R$ ".number_format($valor, 2, ", ", ".");

}

 $imprimir = true;//função condicional

 if($imprimir){

 	function teste(){
 		echo "<h3>Alô Mundo!</h3>";
 	}
 }

//função dentro de função
 function f1(){
 	echo "Here is the function one<br/>";
 	function f2(){
 		echo "Here is the function two<br/><hr>";
 	}
 }

function printArray($array = array()){
	foreach ($array as $item) {
		echo "$item <br/>";
	}
}

$cores = ['Red', 'Green', 'Blue', 'Yellow', 'Black']; 

f1();
f2();
printArray($cores);
teste();
echo "resultado da função: somar: ".somar(30,10);
echo "<br/>Total da compra: ".formatarValor(1500.78);
?>