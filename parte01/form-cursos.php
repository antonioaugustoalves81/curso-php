<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Exemplo de formulário:</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form action="processa-cursos.php" method="get">
		<fieldset>
			<p>
				<input type="checkbox" name="cursos[]" id="c0" value = "PHP"/>
				<label for="nome">PHP</label>
			</p>

			<p>
				<input type="checkbox" name="cursos[]" id="c1" value = "JAVA"/>
				<label for="nome">JAVA</label>
			</p>

			<p>
				<input type="checkbox" name="cursos[]" id="c2" value = "HTML"/>
				<label for="nome">HTML</label>
			</p>

			<p>
				<input type="checkbox" name="cursos[]" id="c3" value = "CSS"/>
				<label for="nome">CSS</label>
			</p>

			<p>
				<input type="checkbox" name="cursos[]" id="c4" value = "JAVASCRIPT"/>
				<label for="nome">JAVASCRIPT</label>
			</p>

			<p>
				<input type="checkbox" name="cursos[]" id="c5" value = "NODE JS"/>
				<label for="nome">NODE JS</label>
			</p>

			<p><input type="submit" value="Confirmar"></p>
		</fieldset>

	</form>
</body>
</html>