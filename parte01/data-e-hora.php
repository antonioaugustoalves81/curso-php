<?php
	echo "<pre>";
	var_dump(date("d/m/Y"));
	var_dump(date("d-m-Y"));
	var_dump(date("Y-m-d"));
	var_dump(date("m/Y"));
	echo "</pre>";
	echo "<h3>Quebrando a data</h3>";
	$dia = date("d");
	$mes = date("m");
	$ano = date("Y");

	echo 'Sapiranga, '.$dia.'/'.$mes.'/'.$ano;
	echo "<h3>Formato para dias:</h3>";

	$dia_semana = date("D");
	echo $dia_semana.'<br>';
	$dia_semana = date("l");
	echo $dia_semana.'<br>';
	$dia_semana = date("N");
	echo $dia_semana.'<br>';
	$dia_semana = date("w");
	echo $dia_semana.'<br>';
	$dia_semana = date("z");
	echo $dia_semana.'<br><hr>';

	echo "<h3>semana e mês:</h3>";
	$semana = date("W");
	echo "Semana numero: ".$semana."<br>";
	$mes = date("F");
	echo "Mês completo: ".$mes."<br>";

	$mes = date("m");
	echo "Mês abreviado: ".$mes."<br>";

	$mes = date("M");
	echo "Mês abreviado II: ".$mes."<br>";
	$mes = date("n");
	echo "Mês abreviado III: ".$mes."<br>";
	$num_dias = date("t");
	echo "Numero de dias do mês atual:". $num_dias;
	echo "<h3>formatos para ano:</h3>";
	$ano = date("Y");
	echo "Ano atual: ".$ano."<br>";

	$ano = date("y");
	echo "Ano abreviado: ".$ano."<br>";

	$ano_bissexto = date("L");
	$ano = date("Y");
	if($ano_bissexto == 1){
		echo "$ano é bissexto";
	}else{
		echo "$ano não é bissexto";
	}

	$ano = date("o");
	echo "<br>Ano atual formato ISO 8601: ".$ano;
?>